require 'rails_helper'

RSpec.describe User, type: :model do
  let(:client) { create(:user) }

  context 'validating required fields' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:message) }
    it { should validate_presence_of(:password) }
    it { should validate_presence_of(:email) }
  end

  it 'object valid with valid attributes' do
    user = build(:user)

    expect(user).to be_valid
  end

  it 'object invalid' do
    user = build(:user)
    user.name = nil

    expect(user).to_not be_valid
  end

  it 'new user always with role client' do
    user = build(:user)
    expect(user.client?).to eq(true)
  end

  context 'Get all invoices' do
    let!(:manager_list) { create_list(:user, 3, :manager) }
    let!(:client_list) { create_list(:user, 2) }

    it 'using scope .client' do
      expect(User.client.count).to eq(client_list.count)
    end

    it 'using scope .mangager' do
      expect(User.manager.count).to eq(manager_list.count)
    end
  end
end