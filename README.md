# Challenge CashWay

# Executando o projeto
1. Instale o ruby `3.0.2` pelo seu gerenciador de ruby.
2. Execute `gem install bundler` para instalar o bundler.
3. Execute `gem install rails -v=6.1.4` para instalar o rails.
3. Execute `bundle install` para instalar todas as gems.
4. Crie o arquivo `.env.local` na pasta raíz do projeto com o conteúdo:

```
DATABASE_URL=postgres://postgres:postgres@db:5432/challenge_cash_development
```

5. Crie o arquivo `.env.test` na pasta raíz do projeto com o conteúdo:
```
DATABASE_URL=postgres://postgres:postgres@db:5432/challenge_cash_test
```

6. E para finalizar, execute `rails server -p 3001` para executar o projeto, pois a porta 3000 deve está sendo executada pelo app Challenge Cash.

# Usa docker?

1. Na pasta raíz tem dois arquivo `Dockerfile` com as informações para gerar a imagem e o `docker-compose.yml` com as definições de orquestrações de todas as imagens necessárias para executar o projeto.
2. Antes de gerar a imagem crie os arquivos `.env.local e .env.test` com a URL do banco de dados mostrando acima.
3. Para gerar a imagem do projeto execute `docker compose build`
4. Execute o comando `docker compose up -d` para iniciar o projeto.
5. Para visualizar o log do rails faço os três passos abaixo:

   5.1 Execute o comando: `docker ps` para listar todos os containers criados pelo comando `docker compose up -d`.

   5.2 Na listagem a primeira coluna vai aparecer o `id do container`, copie do container que tem o nome de `challenge_web_client_web`.

   5.3 Execute o comando: `docker attach ID-OF-CONTAINER` para ter o log em live load (tail)
6. Para executar algum comando do rails digite `docker compose run --rm web SEU-COMANDO-RAILS/SEU-COMANDO-BASH`.

   6.1 Exemplo: Quero ver as rotas `docker compoase run --rm web rails routes`.

# Dependências do projeto

1. Configurar o projeto Challenge Cash https://gitlab.com/frankyston/challenge_cash
2. Este projeto não necessita de criar banco de dados, pois utiliza o banco gerado pelo projeto Challenge Cash.

# Fluxograma do app com o banco de dados

![image info](fluxograma-app-db.png)