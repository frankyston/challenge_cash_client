class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, #:registerable,
         :recoverable, :rememberable, :validatable

  attr_writer :login
  validates :name, :message, presence: true

  enum role: {
    client: 0,
    manager: 1
  }

  def login
    @login || self.account || self.email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["account = :value OR lower(email) = lower(:value)", { :value => login }]).first
    elsif conditions.has_key?(:account) || conditions.has_key?(:email)
      where(conditions.to_h).first
    end
  end
end
